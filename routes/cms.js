const express = require('express');
const router = express.Router();
var cmsController = require('../controllers/cmsController.js')
var cmsAuth =require('../auth/cmsAuth')

// GET Page du CMS
router.get('/login', cmsController.login)
router.get('/logout', cmsAuth, cmsController.logout);
router.get('/', cmsAuth, cmsController.index)
router.get('/article', cmsAuth, cmsController.article)
router.get('/article/add', cmsAuth, cmsController.addArticle)
router.get('/article/edit/:id', cmsAuth, cmsController.editArticle)
router.get('/article/delete/:id', cmsAuth, cmsController.deleteArticle);
router.get('/presentation', cmsAuth, cmsController.presentation)
router.get('/presentation/add', cmsAuth, cmsController.addPresentation)
router.get('/presentation/edit/:id', cmsAuth, cmsController.editPresentation)
router.get('/presentation/delete/:id', cmsAuth, cmsController.deletePresentation);
router.get('/documentation', cmsAuth, cmsController.documentation)


// POST page
router.post('/login', cmsController.loginAdmin)
router.post('/article/add', cmsAuth, cmsController.addArticleBd);
router.post('/article/edit/:id', cmsAuth, cmsController.editArticleBd);
router.post('/presentation/add', cmsAuth, cmsController.addPresentationBd);
router.post('/presentation/edit/:id', cmsAuth, cmsController.editPresentationBd);

module.exports = router;