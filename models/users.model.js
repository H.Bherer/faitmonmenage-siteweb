const mongoose = require('mongoose')

var Schema = mongoose.Schema;

let usersSchema = new Schema({
    first_name: { type: String, required: true },
    last_name: { type: String, required: true },
    email: { type: String, required: true },
    password: { type: String, required: true },
    postal_code: { type: String, required: true },
    phone: { type: String, required: true },
    type: { type: String, required: true }
});

module.exports = mongoose.model("users", usersSchema)


