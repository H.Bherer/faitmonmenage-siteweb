const userModel = require('../models/users.model')

module.exports = {

    connected: async (req, res, next) => {
        
        if (!req.session.authenticated) {
            res.redirect('/login')
        } else {
            res.locals.account = await userModel.findOne({ email: req.session.email })
            next()
        }

    },

    disconnected: (req, res, next) => {
        if(req.session.authenticated) { res.redirect('/profil') }
        else { next() }
    }

}
