const { src, dest, watch, series } = require('gulp');
const sass = require('gulp-sass');
const postcss = require('gulp-postcss');
const terser = require('gulp-terser');
const gulpautoprefixer = require('gulp-autoprefixer');

function jsTask() {
    return src('./app/js/script.js', { sourcemaps: true })
        .pipe(terser())
        .pipe(dest('./public/javascripts', { sourcemaps: '.' }));
}
function jsTask2() {
  return src('./app/js/scriptindex.js', { sourcemaps: true })
      .pipe(terser())
      .pipe(dest('./public/javascripts', { sourcemaps: '.' }));
}
function scssTask() {
    return src('./app/scss/style.scss', { sourcemaps: true })
        .pipe(sass())
        .pipe(dest('./public/stylesheets', { sourcemaps: '.' }));
}
function watchTask(){
    watch(['app/scss/**/*.scss', 'app/js/**/*.js'], series(scssTask, jsTask,));
  }

exports.default = series(
    scssTask,
    jsTask,
    jsTask2,
    watchTask
  );