const { ArticleModel } = require('./../models/articleModel')

module.exports = {
    get: {

        blog: async (req, res, next) => {
            try {
                let document = await ArticleModel.find({})
                res.render('blog', { title: "Faitmonmenage | Blog", articles: document })
            }
            catch (err) {
                console.log(err);
                res.status(500).send("Internal Server error Occured to GET article CMS");
            }
        },

        categoryPresentation: async (req, res, next) => {
            try {
                let document = await ArticleModel.find({category: "Présentation protuis"}).exec();
                res.render('blog', { title: "Faitmonmenage | Blog - Presentation", articles: document })
            }
            catch (err) {
                console.log(err);
                res.status(500).send("Internal Server error Occured to GET article CMS");
            }
        },
        categoryProfil: async (req, res, next) => {
            try {
                let document = await ArticleModel.find({category: "Profil d'employés"}).exec();
                res.render('blog', { title: "Faitmonmenage | Blog - Presentation", articles: document })
            }
            catch (err) {
                console.log(err);
                res.status(500).send("Internal Server error Occured to GET article CMS");
            }
        },
        categoryTrucsAstuces: async (req, res, next) => {
            try {
                let document = await ArticleModel.find({category: "Trucs et astuces"}).exec();
                res.render('blog', { title: "Faitmonmenage | Blog - Presentation", articles: document })
            }
            catch (err) {
                console.log(err);
                res.status(500).send("Internal Server error Occured to GET article CMS");
            }
        },

        article: async (req, res, next) => {
            try {
                let document = await getArticleById(req.params.id);
                res.render('article', { title: "Faitmonmenage | Article", titre: document.title, articles: document })
            }
            catch (err) {
                console.log(err);
                res.status(500).send("Internal Server error Occured to GET article");
            }
        }

    }
}

const getArticleById = async (id) => {
    return await ArticleModel.findById(id);
}