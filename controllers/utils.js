class ArticleEditor {

    data;
    isObject = false;

    constructor(articles) {
        if (!Array.isArray(articles)) { articles = [articles]; this.isObject = true }
        this.data = articles
    }

    convertDate = () => {
        for (let k in this.data) {
            this.data[k].create_date = new Date(parseInt(this.data[k].create_date)).toLocaleDateString()
        }
        return this
    }

    subtractContent = (number) => {
        for (let k in this.data) {
            this.data[k].body = this.data[k].body.substring(0, number) + "..."
        }
        return this
    }

    return = () => {
        if (this.isObject) { this.data = this.data[0] }
        return this.data
    }

}



exports.ArticleEditor = ArticleEditor



exports.server = {
    
    error: (res, err) => {
        console.log(err);
        res.status(500).send("Internal Server error Occured");
    },

}



exports.ajax = {

    redirect: (res, url) => {
        console.log("\x1b[1m \x1b[33m",`[DEV] Ajax: redirection vers ${url}`)
        res.json({ redirect: url })
    },

    error: (res, message) => {
        console.log("\x1b[1m \x1b[31m",`[DEV] Ajax: error => ${message}`)
        res.status(400).json({ error: message })
    },

    serverError: (res, message) => {
        console.log("\x1b[1m \x1b[31m",`[DEV] Ajax: server error => ${message}`)
        res.status(500).json({ error: message, redirect: "/logout" })
    }

}