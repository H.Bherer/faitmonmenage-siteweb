const { AdminModel } = require('./../models/adminModel')
const { ArticleModel } = require('./../models/articleModel')
const { PresentationModel } = require('./../models/presentationModel')
const UserModel = require('./../models/users.model')
const bcrypt = require('bcrypt')

// GET Page CMS
exports.login = async (req, res, next) => {
    try {
        res.render('cms/login', { title: 'Faitmonmenage CMS | CMS', layout: "cms/layoutCMS.hbs" });
    } catch (err) {
        console.log(err);
        res.status(500).send("Internal Server error Occured to GET login CMS");
    }
}
exports.loginAdmin = async (req, res, next) => {
    try {
        const user = await AdminModel.findOne({ email: req.body.email });
        if (user) {
            let compare = await bcrypt.compare(req.body.password, user.password);
            if (compare) {
                req.session.authenticated = true;
                let numArticle = await ArticleModel.count()
                let numPresentation = await PresentationModel.count()
                res.render('cms/', { title: "Faitmonmenage CMS | Accueil", numArticle: numArticle, numPresentation: numPresentation, layout: "cms/layoutCMS.hbs" })
            } else {
                res.render('cms/login', { title: "Faitmonmenage CMS | Connexion", layout: "cms/layoutCMS.hbs", erreur: "Mauvais nom d'utilisateur ou mot de passe" });
            }
        } else {
            res.render('cms/login', { title: "Faitmonmenage CMS | Connexion", layout: "cms/layoutCMS.hbs", erreur: "Mauvais nom d'utilisateur ou mot de passe" });
        }
    } catch (err) {
        console.log(err);
        res.status(500).send("Internal Server error Occured to POST login CMS");
    }
}
exports.logout = async (req, res, next) => {
    try {
        req.session.destroy((err) => {
            res.render('cms/login', { title: "Faitmonmenage CMS | Déconnexion", layout: "cms/layoutCMS.hbs", erreur: "Vous avez bien été déconnecté de votre session!" });
        })

    } catch (err) {
        console.log(err);
        res.status(500).send("Internal Server error Occured to logout");
    }
}

exports.index = async (req, res, next) => {
    try {
        let numArticle = await ArticleModel.count()
        let numPresentation = await PresentationModel.count()
        res.render('cms/index', { title: 'Faitmonmenage CMS | Accueil', numArticle: numArticle, numPresentation: numPresentation, layout: "cms/layoutCMS.hbs" });
    } catch (err) {
        console.log(err);
        res.status(500).send("Internal Server error Occured to GET index CMS");
    }
}

exports.article = async (req, res, next) => {
    try {
        let numArticle = await ArticleModel.count()
        let document = await ArticleModel.find({})
        res.render('cms/articleCMS', { title: 'Faitmonmenage CMS | Article', articles: document, numArticle: numArticle, layout: "cms/layoutCMS.hbs" });
    } catch (err) {
        console.log(err);
        res.status(500).send("Internal Server error Occured to GET article CMS");
    }
}
exports.addArticle = async (req, res, next) => {
    try {
        res.render('cms/add-content/addarticle', { title: 'Faitmonmenage CMS | Ajout Article', layout: "cms/layoutCMS.hbs" });
    } catch (err) {
        console.log(err);
        res.status(500).send("Internal Server error Occured to GET article CMS");
    }
}
exports.editArticle = async (req, res, next) => {
    try {
        let document = await getArticleById(req.params.id);
        res.render('cms/edit-content/editarticle', { title: 'Faitmonmenage CMS | Éditer Article', articles: document, layout: "cms/layoutCMS.hbs" });
    } catch (err) {
        console.log(err);
        res.status(500).send("Internal Server error Occured to GET article CMS");
    }
}
exports.deleteArticle = async (req, res, next) => {
    try {
        await ArticleModel.findByIdAndRemove(req.params.id)
        res.redirect('/cms/article');
    } catch (err) {
        console.log(err);
        res.status(500).send("Internal Server error Occured to delete article");
    }
}

exports.presentation = async (req, res, next) => {
    try {
        let numPresentation = await PresentationModel.count()
        let document = await PresentationModel.find({})
        res.render('cms/presentationCMS', { title: 'Faitmonmenage CMS | Présentation', presentations: document, numPresentation: numPresentation, layout: "cms/layoutCMS.hbs" });
    } catch (err) {
        console.log(err);
        res.status(500).send("Internal Server error Occured to GET article CMS");
    }
}
exports.addPresentation = async (req, res, next) => {
    try {
        res.render('cms/add-content/addpresentation', { title: 'Faitmonmenage CMS | Ajout Présentation', layout: "cms/layoutCMS.hbs" });
    } catch (err) {
        console.log(err);
        res.status(500).send("Internal Server error Occured to GET article CMS");
    }
}
exports.editPresentation = async (req, res, next) => {
    try {
        let document = await getPresentationById(req.params.id);
        res.render('cms/edit-content/editpresentation', { title: 'Faitmonmenage CMS | Éditer Présentation', presentations: document, layout: "cms/layoutCMS.hbs" });
    } catch (err) {
        console.log(err);
        res.status(500).send("Internal Server error Occured to GET article CMS");
    }
}
exports.deletePresentation = async (req, res, next) => {
    try {
        await PresentationModel.findByIdAndRemove(req.params.id)
        res.redirect('/cms/presentation');
    } catch (err) {
        console.log(err);
        res.status(500).send("Internal Server error Occured to delete article");
    }
}

exports.documentation = async (req, res, next) => {
    try {
        res.render('cms/documentation', { title: 'Faitmonmenage CMS | Documentation', layout: "cms/layoutCMS.hbs" });
    } catch (err) {
        console.log(err);
        res.status(500).send("Internal Server error Occured to GET article CMS");
    }
}

exports.profil = (req, res, next) => {
    res.render('cms/profil', { title: 'Faitmonmenage CMS | Profil', layout: "cms/layoutCMS.hbs" });
}

exports.delUser = async (req, res, next) => {
    try {
        await UserModel.findByIdAndRemove(req.params.id)
        res.redirect('/logout');
    } catch (err) {
        console.log(err);
        res.status(500).send("Internal Server error Occured to delete profil");
    }
}

// POST
exports.addArticleBd = async (req, res, next) => {
    try {
        let data = new ArticleModel(req.body);
        data.save(function (err, data) {
            res.redirect('/cms/article')
        })
    } catch (err) {
        console.log(err);
        res.status(500).send("Internal Server error Occured ADD_ARTICLE");
    }
}
exports.editArticleBd = async (req, res, next) => {
    try {
        await ArticleModel.updateOne({ _id: req.params.id }, req.body)
        res.redirect('/cms/article');
    } catch (err) {
        console.log(err);
        res.status(500).send("Internal Server error Occured Edit_ARTICLE");
    }
}
exports.addPresentationBd = async (req, res, next) => {
    try {
        let data = new PresentationModel(req.body);
        data.save(function (err, data) {
            res.redirect('/cms/presentation')
        })
    } catch (err) {
        console.log(err);
        res.status(500).send("Internal Server error Occured ADD_Presentation");
    }
}
exports.editPresentationBd = async (req, res, next) => {
    try {
        await PresentationModel.updateOne({ _id: req.params.id }, req.body)
        res.redirect('/cms/presentation');
    } catch (err) {
        console.log(err);
        res.status(500).send("Internal Server error Occured Edit_ARTICLE");
    }
}

const getArticleById = async (id) => {
    return await ArticleModel.findById(id);
}
const getPresentationById = async (id) => {
    return await PresentationModel.findById(id);
}
const getUserById = async (id) => {
    return await UserModel.findById(id);
}