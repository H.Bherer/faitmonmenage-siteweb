const { validationResult } = require('express-validator')
const { server, ajax } = require('./utils')
const usersQueries = require('../queries/users.queries')
const { UserModel } = require('./../models/users.model')
const { EmployeModel } = require('../models/employeModel')
const { ForfaitModel } = require('./../models/ModelForfait')
const bcrypt = require ('bcrypt');

module.exports = {
    
    get: {

        login: async (req, res) => {
            try {
                res.render('app/auth', { titre: "Connexion", login: true })
            } catch (err) { servErr(res, err) }
        },

        register: (req, res) => {
            res.render('app/auth', { titre: "Inscription", register: true })
        },

        logout: (req, res) => {
            req.session.authenticated = false
            res.redirect('/login')
        },

        hiring: async (req, res, next) => {
            let forfaits = await ForfaitModel.find({})
            res.render('app/embauche', {titre: "Embauche", forfaits})
        },
        
        profil: async (req, res) => {
            let user = await usersQueries.getUserByEmail(req.session.email)
            res.render('app/profil', { titre: "Profil", user })
        },

        profilEdit: async (req, res) => {
            let user = await usersQueries.getUserByEmail(req.session.email)
            res.render('app/profil', { titre: "Éditer votre profil", edit: true, user })
        },
    },




    post: {

        login: async (req, res) => {
            try{    
                let user = await usersQueries.getUserByEmail(req.body.email)
                if(user){
                    let compare = await bcrypt.compare(req.body.password, user.password)
                    if(compare) {
                        req.session.email = req.body.email
                        req.session.authenticated = true
                        ajax.redirect(res, '/profil')
                    }else{ ajax.error(res, "Courriel ou mot de passe invalide") }
                }else{ ajax.error(res, "Courriel ou mot de passe invalide") }
            }catch(err) { server.error(res, err) }
        },

        register: async (req, res) => {
            try{
                let user = await usersQueries.getUserByEmail(req.body.email)
                if(!user){
                    req.body.password = await bcrypt.hash(req.body.password, 10);
                    usersQueries.addUser(req.body)
                    ajax.redirect(res, '/login')
                }else{ ajax.error(res, "Ce courriel est déjà utilisé") }
            }catch(err) { server.error(res, err) }
        },

        hiring: (req, res) => { }

    },




    put: {

        profilEdit: async (req, res) => {
            try{
                let user = await usersQueries.getUserByEmail(req.session.email)
                if(user){
                    if(req.body.email != req.session.email) { 
                        let checkEmail = await usersQueries.getUserByEmail(req.body.email)
                        if(checkEmail){ ajax.error(res, "Ce courriel est déjà utilisé"); return }
                    }
                    usersQueries.updateUser(req.session.email, req.body)
                    req.session.email = req.body.email
                    ajax.redirect(res, '/profil')
                }else{ ajax.serverError(res, "ERROR: Compte innexistant") }
            }catch(err) { server.error(res, err) }
        },

    }
}

