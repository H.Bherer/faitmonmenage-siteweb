const usersModel = require('../models/users.model');

module.exports = {
    
    getUserByEmail: (email) => {
        return usersModel.findOne({ email: email }).exec();
    },

    addUser: (data) => { 
        const newUser = new usersModel({
            first_name: data.first_name,
            last_name: data.last_name,
            email: data.email,
            password: data.password,
            postal_code: data.postal_code,
            phone: data.phone,
            type: data.type
        })
        return newUser.save()
    },

    updateUser: (email, data) => {
        return usersModel.findOneAndUpdate({ email: email }, { 
            last_name: data.last_name, 
            first_name: data.first_name, 
            email: data.email, 
            postal_code: data.postal_code, 
            phone: data.phone,  
            type: data.type
        }).exec()
    }

}