module.exports = (req, res, next) =>{
    if (!req.session.authenticated){
        res.redirect('/cms/login');
    } else {
        next();
    }
}